import { createRouter, createWebHashHistory } from 'vue-router';
import AppLayout from '@/layout/AppLayout.vue';
import { useStore } from 'vuex';
import { ref } from 'vue';

const department_name = ref(null);
const department_id = ref(null);

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/auth/login',
            name: 'login',
            component: () => import('@/views/pages/auth/Login.vue')
        },
        {
            path: '/',
            component: AppLayout,
            children: [
                {
                    path: '/',
                    name: 'dashboard',
                    component: () => import('@/views/Dashboard.vue'),
                    meta: { requiresAuth: true, title: 'Halaman Utama' }
                },
                {
                    path: '/inbound',
                    name: 'inbound',
                    component: () => import('@/views/layout/inbound/InboundPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/inbound/:id',
                    name: 'inbound/id',
                    component: () => import('@/views/layout/inbound/InbounddetailPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'admin'
                        }
                    }
                },
                {
                    path: '/outbound',
                    name: 'outbound',
                    component: () => import('@/views/layout/outbound/OutboundPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'admin'
                        }
                    }
                },
                {
                    path: '/outbound/:id',
                    name: 'outbound/id',
                    component: () => import('@/views/layout/outbound/OutbounddetailPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'admin'
                        }
                    }
                },
                {
                    path: '/outbound-overview',
                    name: 'outbound-overview',
                    component: () => import('@/views/layout/outbound/OutboundPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'overview'
                        }
                    }
                },
                {
                    path: '/outbound-request',
                    name: 'outbound-request',
                    component: () => import('@/views/layout/outbound/OutboundoverPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'request'
                        }
                    }
                },
                {
                    path: '/master-material',
                    name: 'master-material',
                    component: () => import('@/views/layout/master/MaterialPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-storagebin',
                    name: 'master-storagebin',
                    component: () => import('@/views/layout/master/StoragebinPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-slocation',
                    name: 'master-slocation',
                    component: () => import('@/views/layout/master/SlocationPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-department',
                    name: 'master-department',
                    component: () => import('@/views/layout/master/DepartmentPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-cost',
                    name: 'master-cost',
                    component: () => import('@/views/layout/master/CostPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-movement',
                    name: 'master-movement',
                    component: () => import('@/views/layout/master/MovementPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-recipient',
                    name: 'master-recipient',
                    component: () => import('@/views/layout/master/RecipientPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-uom',
                    name: 'master-uom',
                    component: () => import('@/views/layout/master/UomPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/master-category',
                    name: 'master-category',
                    component: () => import('@/views/layout/master/AdjcategoryPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/material-loc',
                    name: 'material-loc',
                    component: () => import('@/views/layout/MateriallogPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/adjustment',
                    name: 'adjustment',
                    component: () => import('@/views/layout/inventory/AdjustmentPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/adjustment/form-adjustment',
                    name: 'adjustment/form',
                    component: () => import('@/views/layout/inventory/AdjustmentformPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/adjustment/:id',
                    name: 'adjustment/id',
                    component: () => import('@/views/layout/inventory/AdjustmentdetailPage.vue'),
                    meta: {
                        requiresAuth: true,
                        dataToSend: {
                            value: 'admin'
                        }
                    }
                },
                {
                    path: '/user',
                    name: 'user',
                    component: () => import('@/views/layout/UserPage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/role',
                    name: 'role',
                    component: () => import('@/views/layout/RolePage.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/formlayout',
                    name: 'formlayout',
                    component: () => import('@/views/uikit/FormLayout.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/input',
                    name: 'input',
                    component: () => import('@/views/uikit/Input.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/floatlabel',
                    name: 'floatlabel',
                    component: () => import('@/views/uikit/FloatLabel.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/invalidstate',
                    name: 'invalidstate',
                    component: () => import('@/views/uikit/InvalidState.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/button',
                    name: 'button',
                    component: () => import('@/views/uikit/Button.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/table',
                    name: 'table',
                    component: () => import('@/views/uikit/Table.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/list',
                    name: 'list',
                    component: () => import('@/views/uikit/List.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/tree',
                    name: 'tree',
                    component: () => import('@/views/uikit/Tree.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/panel',
                    name: 'panel',
                    component: () => import('@/views/uikit/Panels.vue'),
                    meta: { requiresAuth: true }
                },

                {
                    path: '/uikit/overlay',
                    name: 'overlay',
                    component: () => import('@/views/uikit/Overlay.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/media',
                    name: 'media',
                    component: () => import('@/views/uikit/Media.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/menu',
                    component: () => import('@/views/uikit/Menu.vue'),
                    children: [
                        {
                            path: '/uikit/menu',
                            component: () => import('@/views/uikit/menu/PersonalDemo.vue'),
                            meta: { requiresAuth: true }
                        },
                        {
                            path: '/uikit/menu/seat',
                            component: () => import('@/views/uikit/menu/SeatDemo.vue'),
                            meta: { requiresAuth: true }
                        },
                        {
                            path: '/uikit/menu/payment',
                            component: () => import('@/views/uikit/menu/PaymentDemo.vue'),
                            meta: { requiresAuth: true }
                        },
                        {
                            path: '/uikit/menu/confirmation',
                            component: () => import('@/views/uikit/menu/ConfirmationDemo.vue'),
                            meta: { requiresAuth: true }
                        }
                    ]
                },
                {
                    path: '/uikit/message',
                    name: 'message',
                    component: () => import('@/views/uikit/Messages.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/file',
                    name: 'file',
                    component: () => import('@/views/uikit/File.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/charts',
                    name: 'charts',
                    component: () => import('@/views/uikit/Chart.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/uikit/misc',
                    name: 'misc',
                    component: () => import('@/views/uikit/Misc.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/blocks',
                    name: 'blocks',
                    component: () => import('@/views/utilities/Blocks.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/utilities/icons',
                    name: 'icons',
                    component: () => import('@/views/utilities/Icons.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/pages/timeline',
                    name: 'timeline',
                    component: () => import('@/views/pages/Timeline.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/pages/empty',
                    name: 'empty',
                    component: () => import('@/views/pages/Empty.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/pages/crud',
                    name: 'crud',
                    component: () => import('@/views/pages/Crud.vue'),
                    meta: { requiresAuth: true }
                },
                {
                    path: '/documentation',
                    name: 'documentation',
                    component: () => import('@/views/utilities/Documentation.vue'),
                    meta: { requiresAuth: true }
                }
            ]
        },
        {
            path: '/landing',
            name: 'landing',
            component: () => import('@/views/pages/Landing.vue')
        },
        {
            path: '/pages/notfound',
            name: 'notfound',
            component: () => import('@/views/pages/NotFound.vue')
        },
        {
            path: '/auth/access',
            name: 'accessDenied',
            component: () => import('@/views/pages/auth/Access.vue')
        },
        {
            path: '/auth/error',
            name: 'error',
            component: () => import('@/views/pages/auth/Error.vue')
        }
    ]
});

router.beforeEach(async (to, from, next) => {
    const store = useStore();

    // Cek apakah rute memerlukan autentikasi
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        // Cek apakah pengguna telah login (gunakan kondisi yang sesuai)
        const isAuthenticated = localStorage.getItem('token') !== null;

        if (!isAuthenticated) {
            // Jika belum login, redirect ke halaman login
            next('/auth/login');
        } else {
            const token = localStorage.getItem('token');
            const tokenData = JSON.parse(atob(token.split('.')[1])); // Decode token
            const expirationTimestamp = tokenData.exp * 1000; // Convert to milliseconds
            const expirationDate = new Date(expirationTimestamp);

            // Tampilkan tanggal kedaluwarsa (misalnya, dalam konsol)
            console.log('Token expired on:', expirationDate);

            // Cek apakah token sudah kedaluwarsa
            if (expirationDate <= new Date()) {
                // Hapus token dari penyimpanan lokal
                localStorage.removeItem('token');
                store.commit('resetState');

                // Jika sudah kedaluwarsa, redirect ke halaman login
                next('/auth/login');
            } else {
                await store.dispatch('fetchDepartmentData');
                // Now, department data is set in the store
                // You can access it using store.state.department_name and store.state.department_id
                department_name.value = store.state.department_name;
                department_id.value = store.state.department_id;
                // Jika belum kedaluwarsa, izinkan navigasi
                next();
            }
        }
    } else {
        if (to.path === '/auth/login' && localStorage.getItem('token') !== null) {
            // Jika pengguna sudah login, redirect ke halaman beranda atau halaman lain
            next('/');
        } else {
            next();
        }
    }
});

export default router;
